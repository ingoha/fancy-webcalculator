/**
 * Created by tammschw on 29/05/15.
 */

function getCalculationFromServer (num, operation) {
    var parse = {};
    parse.num = num;
    parse.operation = operation;

    console.log(JSON.stringify(parse));

	// Problem here is that the return inside the anonymous functions are not
	// the same as the return of the outer (getCalculationFromServer) function.
	// s. https://stackoverflow.com/questions/20722421/how-to-pass-the-jquerys-ajax-response-data-to-outer-function

	// One possible solution is to process the result directly inside the
	// callback function. This is good JS practice - JS doesn't use the 
	// procedural function call, return result model but the event based model...

    $.ajax({
        url: '/calc',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(parse),
        success: function (data) {
            console.log(data);
			$("#trigResult").html(data);
            return data;
        },
        error: function (xhr, status, error) {
            console.log('Error: ' + error.message);
            return "Error while communicate to the server";
        }
    });
}
